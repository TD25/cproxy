#!/usr/bin/env python

import socket
import time

def send_str(socket, string):
    b = str.encode(string)
    socket.send(b)
    # delay for 1 second so that multiple strs aren't buffered and sent as 1 segment
    time.sleep(1)

def print_response(socket):
    while (1):
        buf = socket.recv(512);
        print(buf.decode("utf-8"))

TCP_IP = '127.0.0.1'
TCP_PORT = 8080
BUFFER_SIZE = 1024
msg1 = """GET /first/message/in/segment/1 HTTP/1.1
Host: h
Connection:keep-alive

GET /second/message/in/segment/1 HTTP/1.1
Header: value

"""

msg20 = "GET "
msg21 = "message/over/multiple/segments HTTP/1.1\n\n"

msg30 = """POST headers/over/multiple/segments HTTP/2.0
Host: tadas
Conn"""
msg31 = """ection: keep-alive, 300
some-header: val"""
msg32 = """ue
some-header2: value2
"""
msg33 = """last-header: last value

"""

msg40 = """OPTION headers/over/multiple/segments/2 HTTP/1.1
"""
msg41 = """header1: doesn't have a newline"""
msg42 = """
header2: have newlines

"""

msg50 = "GET"
msg51 = "message/over/multiple/segments/bad-format HTTP/1.1\n\n"

msg60 = """HTTP/1.1 wrong type first
GET aaaa HTTP/1.1

"""

msg70 = """SOMEMETHOD www.krepsinis.net:80 HTTP/1.1
Host: www.krepsinis.net:80

GET http://www.krepsinis.net/ HTTP/1.1
Host: www.krepsinis.net

"""

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

#test1: multiple messages per segment
send_str(s, msg1)

#test2: one message over multiple segments
send_str(s, msg20)
send_str(s, msg21)

#test3: headers over multple segments
print("msg30 length: ", len(msg30))
send_str(s, msg30)
send_str(s, msg31)
send_str(s, msg32)
send_str(s, msg33)

#test4:
send_str(s, msg40)
send_str(s, msg41)
send_str(s, msg42)

#test5: should fail
send_str(s, msg50);
send_str(s, msg51);

#test6:
send_str(s, msg60);

#test7: CONNECT, send request, and forward response
send_str(s, msg70);

print_response(s);

#data = s.recv(BUFFER_SIZE)
s.close()


#:w
#print ("received data:", data)
