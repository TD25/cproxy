import socket
import time

def send_str(socket, string):
    b = str.encode(string)
    socket.send(b)
    # delay for 1 second so that multiple strs aren't buffered and sent as 1 segment
    time.sleep(1)

def print_response(socket):
    while (1):
        buf = socket.recv(512);
        print(buf.decode("utf-8"))

TCP_IP = '127.0.0.1'
TCP_PORT = 8080
BUFFER_SIZE = 1024

# simple content length
msg1 = """GET / HTTP/1.1
Host: www.krepsinis.net

"""

msg2 = """GET /info/ HTTP/1.1
Host: www.cplusplus.com

"""

#msg1 = """GET /lt3/ HTTP/1.1
#Host: www.mif.vu.lt
#Connection: keep-alive
#Accept: text/html,application/xhtml+xml
#
#"""
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
#
send_str(s, msg1);
send_str(s, msg2);
print_response(s);
print_response(s);

#data = s.recv(BUFFER_SIZE)
s.close()


#:w
#print ("received data:", data)
