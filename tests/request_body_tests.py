import socket
import time

def send_str(socket, string):
    b = str.encode(string)
    socket.send(b)
    # delay for 1 second so that multiple strs aren't buffered and sent as 1 segment
    time.sleep(1)

def print_response(socket):
    while (1):
        buf = socket.recv(512);
        print(buf.decode("utf-8"))

TCP_IP = '127.0.0.1'
TCP_PORT = 8080
BUFFER_SIZE = 1024

# simple content length
msg1 = """GET www.krepsinis.net/1 HTTP/1.1
Host: www.krepsinis.net
Content-length: 5

aaa"""
msg11 = "aa"

# content length, with another message
msg2 = """GET www.krepsinis.net/2 HTTP/1.1
Host: www.krepsinis.net
Content-length: 5

aaaaaGET another/message/with/body HTTP/1.1
Host: www.krepsinis.net

"""
#simple chunked
#FIXME: body
msg3 = """GET www.krepsinis.net/3 HTTP/1.1
Host: www.krepsinis.net
transfer-encoding: chunked

E
simple chunked
0

"""

# multiple encodings, last chunked. Extension. Multiple chunks
msg4 = """GET www.krepsinis.net/4 HTTP/1.1
Host: www.krepsinis.net
transfer-encoding: aaa, chunked

5 ;multiple encodings, 2 chunks
aaaaa
2 ;second chunk
bb
0

"""

# multiple encodings, last not chunked (error)
msg5 = """GET www.krepsinis.net/5 HTTP/1.1
Host: www.krepsinis.net
transfer-encoding: chunked, bb,ccc

last encoding not chunkded"""

# override content-length
msg6 = """GET www.krepsinis.net/6 HTTP/1.1
Host: www.krepsinis.net
transfer-encoding: aaa,bbb,chunked
Content-Length: 12

4 ;override content length
aaaa
0

"""

#multiple content length (error)
msg7 = """GET www.krepsinis.net/7 HTTP/1.1
Host: www.krepsinis.net
Content-Length: 5
Content-Length: 2

multiple content lengths (should be error)"""

msg81 = """GET www.krepsinis.net/8 HTTP/1.1
Host: www.krepsinis.net
transfer-encoding: chunked, bb,ccc
transfer-encoding: dd,chunked

4; chunk per multiple segments and multiple encoding headers
"""
msg82 = "aa"
msg83 = """aa
5 ;another chunk per multiple segments
bbb"""
msg84 = """bb
"""
msg85 = """0;last chunk on the last segment

"""

msg9 = """GET /9 HTTP/1.1
Host: www.krepsinis.net
transfer-encoding: chunked

1; multiple chunked messages on one segment
a
2
aa
0

GET /9 HTTP/1.1
Host: www.krepsinis.net
transfer-encoding: chunked

1; multiple chunked messages on one segment (2)
a
6
aaaaaa
2
aa
0

"""

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
#
send_str(s, msg1);
send_str(s, msg11);
send_str(s, msg2);
send_str(s, msg3);
send_str(s, msg4);
#send_str(s, msg5);
send_str(s, msg6);
#send_str(s, msg7);
send_str(s, msg81);
send_str(s, msg82);
send_str(s, msg83);
send_str(s, msg84);
send_str(s, msg85);
send_str(s, msg9);

print_response(s);

#data = s.recv(BUFFER_SIZE)
s.close()


#:w
#print ("received data:", data)
