#!/usr/bin/env python

import socket
import time

def send_str(socket, string):
    b = str.encode(string)
    socket.send(b)
    # delay for 1 second so that multiple strs aren't buffered and sent as 1 segment
    time.sleep(1)



TCP_IP = '127.0.0.1'
TCP_PORT = 8080
BUFFER_SIZE = 1024
msg1 = """HTTP/1.1 200 First message in one segment
Host: tadas
Connection: keep-alive

HTTP/1.1 201 Second message in one segment
some-header-name: value

"""

msg20 = "HTTP/1.1"
msg21 = " 200 message over multiple segments \n\n"


msg30 = """HTTP/1.1 300 headers over multiple segments
Host: tadas
Conn"""
msg31 = """ection: keep-alive, 300
some-header: val"""
msg32 = """ue
some-header2: value2
"""
msg33 = """last-header: last value

"""

msg40 = """HTTP/1.1 400 headers over multiple segments 2
"""
msg41 = """header1: doesn't have a newline"""
msg42 = """
header2: have newlines

"""

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

#test1: multiple messages per segment
send_str(s, msg1)

#test2: one message over multiple segments
send_str(s, msg20)
send_str(s, msg21)

#test3: headers over multple segments
print("msg30 length: ", len(msg30))
send_str(s, msg30)
send_str(s, msg31)
send_str(s, msg32)
send_str(s, msg33)

#test4:
send_str(s, msg40)
send_str(s, msg41)
send_str(s, msg42)

#data = s.recv(BUFFER_SIZE)
s.close()


#:w
#print ("received data:", data)
