#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <signal.h>
#include <thread>
#include "spdlog/spdlog.h"
#include "cproxy.h"
#include "common.h"


using namespace std;

// Creates stream socket, binds it to port and listens
int createListenerSocket(const char* port, int backlog) 
{
    addrinfo hints, *ai, *p;
    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;	//allow IPv6 as well as IPv4
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;	//listen through all interfaces
	int rv, listener;
	int yes = 1;
    if ((rv = getaddrinfo(NULL, port, &hints, &ai)) != 0) {
		cerr << gai_strerror(rv) << endl;
        exit(1);
    }
    
    for(p = ai; p != NULL; p = p->ai_next) {
        listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listener < 0) { 
            continue;
        }
        
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

        if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
			cerr << strerror(errno) << endl;
            close(listener);
            continue;
        }

        break;
    }

    if (p == NULL) {
		// if we got here, it means we didn't get bound
        cerr << "cproxy: failed to bind" << endl;
        exit(1);
    }

    freeaddrinfo(ai); // all done with this

    if (listen(listener, backlog) == -1) {
		cerr << "listen: strerror(errno)";
        exit(3);
    }
	return listener;
}

void handleNewClient(int fd, sockaddr_storage clientAddr)
{
	CProxy proxy = CProxy(fd, clientAddr);
	proxy.serviceClient();
}

int run(int listener) 
{
    int newfd;							// newly accept()ed socket descriptor
    struct sockaddr_storage remoteaddr; // client address
    socklen_t addrlen = sizeof remoteaddr;
	auto logger = spdlog::get("global");

	char remoteIP[INET6_ADDRSTRLEN];

    // main loop
    while(1) 
	{
		newfd = accept(listener, (sockaddr*)&remoteaddr, &addrlen);
		if (newfd == -1)
			logger->error(strerror(errno));
		else
		{
			logger->info("New connection: from {0} on {1:d}", 
					inet_ntop(remoteaddr.ss_family, get_in_addr((sockaddr*)&remoteaddr),
					remoteIP, INET6_ADDRSTRLEN), newfd);
			std::thread(handleNewClient, newfd, remoteaddr).detach();
		}
    } 
    return 0;
}

int main(void)
{
	//TODO: read configuration from file
	try
	{
		// use spd::get("logger name") to get logger from anywhere
		auto globalLogger = spdlog::rotating_logger_mt("global", "logs/global", 1048576 * 5, 3);
		//TODO: read logging level from file
		globalLogger->set_level(spdlog::level::debug);
		globalLogger->flush_on(spdlog::level::debug);	//TODO: change to warn
		globalLogger->info("Logger created");
	}
	// Exceptions will only be thrown upon failed logger or sink construction (not during logging)
    catch (const spdlog::spdlog_ex& ex)
    {
        cerr << "Log init failed: " << ex.what() << std::endl;
        return 1;
    }
	int listener = createListenerSocket(PORT, BACKLOG);
	spdlog::get("global")->info("Listener created");
	int r = run(listener);
	spdlog::drop_all();
	return r;
}
