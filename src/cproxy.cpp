#include "cproxy.h"
#include "client_connection.h"
#include "server_connection.h"
#include "cproxy_exception.h"
#include <unordered_map>
#include <sys/select.h>

void CProxy::serviceClient()
{
	try
	{
		fd_set readFds;
		FD_ZERO(&m_socketSet);
		FD_ZERO(&readFds);

		ClientConnection* con = new ClientConnection(m_clientSocket, m_remoteIP, this);
		addConnection(con);

		//TODO: read from config file
		timeval timeout;
		timeout.tv_usec = 0;

		while (1)
		{
			readFds = m_socketSet;
			timeout.tv_sec = TIMEOUT;
			if (select(m_fdmax+1, &readFds, NULL, NULL, &timeout) == -1) {
				m_logger->error("{}:{}: {}", m_remoteIP, std::to_string(m_clientSocket), strerror(errno));
				return;
			}
			bool noneSet = true;
			for (auto it = m_connections.begin(); it != m_connections.end(); it++)
			{
				Connection* c = it->second;
				int socket = c->getSocket();
				if (FD_ISSET(socket, &readFds))
				{
					ssize_t n = c->receiveMessage();
					if (n == 0)
					{
						if (!removeConnection(socket))
							return;
					} 
					else if (n < 0) //error occured
					{
						int err = errno;
						m_logger->error("{}:{}: receiveMessage: {}. Socket: {}", m_remoteIP, 
								std::to_string(m_clientSocket), strerror(errno), std::to_string(socket));
						// always close connection except when would block error occured
						if (err != EAGAIN && err != EWOULDBLOCK)
						{
							if (!removeConnection(socket))
								return;
						}
					}
					else
					{
						c->handleMessage();
						if (c->toClose())
						{
							if (!removeConnection(socket))
								return;
						}
					}
					noneSet = false;
					break;
				}
			}
			if (noneSet)	// means timout expired
			{
				spdlog::get("global")->debug("{0}:{1:d}: timeout expired", m_remoteIP, m_clientSocket); 
				break;		// TODO: make sure this object is destroyed, return status
			}
		}
	}
	catch (std::exception& exc)
	{
		spdlog::get("global")->error("{}: CProxy::serviceClient: {}. Thread exiting.", exc.what());
		return;
	}
}


int CProxy::findBiggestFileDescriptor() const
{
	int max = -1;
	for (auto it = m_connections.begin(); it != m_connections.end(); it++)
	{
		if (it->first > max)
			max = it->first;
	}
	return max;
}

// returns false if thread should terminate
bool CProxy::removeConnection(int socket)
{
	auto it = m_connections.find(socket);
	assert(it != m_connections.end());

	// if it's not a client socket, we can continue most of the time
	if (m_clientSocket != socket)
	{
		// if we are blindly forwarding then we have to disconnect client as well
		auto type = it->second->getTransmissionType();
		if (type == Connection::TransmissionType::BLIND_FORWARD ||
				type == Connection::TransmissionType::BODY_FORWARD_AND_CLOSE)
		{
			removeAllConnections();
			return false;
		}
		// else notify client about closed server connection
		auto clientIt = m_connections.find(m_clientSocket);
		assert(clientIt != m_connections.end());
		//FIXME:
		(static_cast<ClientConnection*>(clientIt->second))->removeServerConnection(
				(static_cast<ServerConnection*>(it->second))->getHostName());

		if (close(socket) != 0)
			throw CProxyException("Failed to close socket " + socket, "", strerror(errno));
		
		delete it->second;
		m_connections.erase(socket);

		// If we are removing biggest file descriptor find next biggest
		if (m_fdmax == socket)
			m_fdmax = findBiggestFileDescriptor();
		FD_CLR(socket, &m_socketSet);
		m_logger->info("{0}: {1} {2:d}", m_remoteIP, " connection removed, socket: ", socket);
		return true;
	}
	else  // if removing client there is not point to continue, so remove all connections and quit
	{
		removeAllConnections();
		return false;
	}
}

bool CProxy::removeConnection(Connection* connection)
{
	return removeConnection(connection->getSocket());
}

void CProxy::removeAllConnections()
{
	//delete and close all allocated connections
	for (auto it = m_connections.begin(); it != m_connections.end(); it++)
	{
		close(it->second->getSocket());
		delete it->second;
	}
	m_connections.clear();
	m_logger->info("{}:{}: {}", m_remoteIP, std::to_string(m_clientSocket), " all connections removed");
}

void CProxy::addConnection(Connection* connection)
{
	int socket = connection->getSocket();
	auto pair = m_connections.emplace(socket, connection);
	assert(pair.second);
	m_connections[socket] = connection;
	// if that's not client connection we are adding, notify client that new server is present
	if (socket != m_clientSocket)
	{
		//FIXME:
		(static_cast<ClientConnection*>(m_connections[m_clientSocket]))->addServerConnection(
				static_cast<ServerConnection*>(connection));
	}

	FD_SET(socket, &m_socketSet);
	//TODO: setsockopt to set internal buffer size to bigger
	// keep track of the biggest file descriptor
	m_fdmax = (m_fdmax < socket) ? socket : m_fdmax;

	m_logger->info("{0}:{1:d}: {2} {3:d}", m_remoteIP, m_clientSocket, 
			" new connection registered, socket: ", socket);
}

CProxy::~CProxy()
{
	if (!m_connections.empty())
		removeAllConnections();	
}


