#include "sbuffer.h"
#include <cstring>
#include <unistd.h>

void SBuffer::freeFirstN(size_t n)
{
	assert(n > 0);
	size_t used = getUsedSpace();
	assert(used >= n);

	// if not freeing all used space, we need to put remaning bytes in the begining
	if (used != n)
		memmove(m_buff, m_buff + n, used - n);

	m_start -= n;
}

ssize_t SBuffer::writeTo(int fd, size_t count)
{
	size_t used = getUsedSpace();
	assert(used >= count);
	ssize_t r = write(fd, m_buff, count);
	// free writen data in buffer
	if (r > 0)
		freeFirstN(r);
	return r;
}

ssize_t SBuffer::readFrom(int fd, size_t count)
{
	size_t free = getFreeSpace();
	assert(free >= count);
	ssize_t r = read(fd, getFreeStart(), count);
	if (r > 0)
		useFirstN(r);
	return r;
}

bool SBuffer::writeAllTo(int fd, size_t count, size_t* written)
{
	size_t used = getUsedSpace();
	assert(count > 0);
	assert(used >= count);
	*written = 0;
	size_t bytesLeft = count;
	int r;

	while (*written < count)
	{
		// Writen space gets freed
		r = write(fd, m_buff+*written, bytesLeft);
		if (r < 0)
			break;
		*written += r;
		bytesLeft -= r;
	}
	// Free written data
	if (*written > 0)
		freeFirstN(*written);
	return r < 0 ? false : true;
}

bool SBuffer::sendAllTo(int fd, size_t count, size_t* written, int flags)
{
	size_t used = getUsedSpace();
	assert(count > 0);
	assert(used >= count);
	*written = 0;
	size_t bytesLeft = count;
	int r;

	while (*written < count)
	{
		// Writen space gets freed
		r = send(fd, m_buff+*written, bytesLeft, flags);
		if (r < 0)
			break;
		*written += r;
		bytesLeft -= r;
	}
	// Free written data
	if (*written > 0)
		freeFirstN(*written);
	return r < 0 ? false : true;

}
