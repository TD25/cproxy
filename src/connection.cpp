#include "connection.h"
#include "cproxy_exception.h"
#include <stdexcept>
#include <system_error>

Connection::Connection(int socket) : m_socket(socket)
{
}

Connection::~Connection()
{
}

ssize_t Connection::receiveMessage()
{
	// Don't block read
	int flags = fcntl(m_socket, F_GETFL);
	fcntl(m_socket, F_SETFL, flags & (~O_NONBLOCK)); 
	int r = m_inBuff.readFrom(m_socket, m_inBuff.getFreeSpace());
	// reset it because we want writes to block
	fcntl(m_socket, F_SETFL, flags);
	return r;
}

void Connection::sendAll(SBuffer& buffer, size_t bytes)
{
	size_t sentBytes;
	// set MSG_NOSIGNAL flag so that process does not get SIGPIPE if pipe is broken
	if (!buffer.sendAllTo(m_socket, bytes, &sentBytes, MSG_NOSIGNAL))
	{
		// Close on failed write. Expecting blocking sockets.
		m_close = true;
		throw CProxyException("Connection::sendAll: write returned error", "",
				strerror(errno));
	}
}

void Connection::sendRequest(const HttpParser::Request& request)
{
	// We don't handle case where there are messages already in buffer
	assert(m_outBuff.getUsedSpace() == 0);
	// add body headers
	size_t used = 0;
	m_parser.packRequest(&request, m_outBuff.getFreeStart(), m_outBuff.getFreeSpace(), &used);
	m_outBuff.useFirstN(used);
	// write until whole message is sent
	sendAll(m_outBuff, used);
}


void Connection::sendResponse(const HttpParser::Response& response)
{
	// We don't handle case where there are messages already in buffer
	assert(m_outBuff.getUsedSpace() == 0);
	size_t used = 0;
	// throws std::length_exception
	m_parser.packResponse(&response, m_outBuff.getFreeStart(), m_outBuff.getFreeSpace(), &used);
	m_outBuff.useFirstN(used);
	// write until whole message is sent
	sendAll(m_outBuff, used);
}

void Connection::forwardBody(Connection* conn)
{
	assert(m_bodyLength > 0 && m_sentBytes >= 0);
	size_t remaining = m_bodyLength - m_sentBytes;
	assert(remaining > 0);
	size_t buffBytes = m_inBuff.getUsedSpace();
	size_t bytes = (buffBytes < remaining) ? buffBytes : remaining;
	conn->sendAll(m_inBuff, bytes);
	m_sentBytes += bytes;
}

void Connection::setBodyTransferType(Request& req)
{
	//https://tools.ietf.org/html/draft-ietf-httpbis-p1-messaging-14#section-3.3
	HttpParser::ValueList codings = m_parser.findValueList(req.headers, "transfer-encoding");
	if (!codings.empty())
	{
		std::string& last = codings.back();
		// There might be space before token, remove it
		last.erase(remove_if(last.begin(), last.end(), isspace), last.end());
		if (last == "chunked")
		{
			// transfer-encoding header overrides content-length header, so remove it if present
			req.headers.erase("content-length");
			m_type = CHUNKED_BODY_FORWARD;
			resetBody();
			m_newChunk = true;
			spdlog::get("global")->debug("{} set body transfer type to chunked", m_logSpec);
		}
		else
		{
			m_close = true;
			// if last encoding is not chunked, we can't handle this request, 
			// because we can't determine length of the message
			throw CProxyException("Can't parse message with encoding chunked as not the last", 
					"400");
		}
	}
	else
	{
		size_t count = req.headers.count("content-length");
		if (count > 1)
		{
			m_close = true;
			throw CProxyException("Multiple content-length header fields received", 
					"400");
		}
		if (count == 0)
			m_type = NORMAL;	// no body
		else
		{
			int size;
			try
			{
				auto it = req.headers.find("content-length");
				size = std::stoi(it->second);
			}
			catch (std::logic_error& err) //invalid_argument or out_of_range
			{
				m_close = true;
				throw CProxyException("Non integer value passed as content-length", 
						"400");
			}
			if (size < 0)
			{
				m_close = true;
				throw CProxyException("Content-length can't be less than 0", "400");
			}
			else if (size == 0)
			{
				m_type = NORMAL;	// no body
				return;
			}

			m_bodyLength = size;
			m_sentBytes = 0;
			m_type = BODY_FORWARD;
			spdlog::get("global")->debug("{} body transfer type set to simple body forward", 
					m_logSpec);
		}
	}
}

void Connection::setBodyTransferType(Response& resp, const std::string& reqMethod,
		Connection* receiver)
{
	// https://tools.ietf.org/html/draft-ietf-httpbis-p1-messaging-14#section-3.3
	if (reqMethod == "HEAD" || resp.statusCode[0] == '1' || resp.statusCode == "204" ||
			resp.statusCode == "304")
	{
		m_type = NORMAL;
	}
	else
	{
		HttpParser::ValueList codings = m_parser.findValueList(resp.headers, "transfer-encoding");
		if (!codings.empty())
		{
			std::string& last = codings.back();
			// There might be space before token, remove it
			last.erase(remove_if(last.begin(), last.end(), isspace), last.end());
			if (last == "chunked")
			{
				// transfer-encoding header overrides content-length header, so remove it if present
				resp.headers.erase("content-length");
				m_type = CHUNKED_BODY_FORWARD;
				resetBody();
				m_newChunk = true;
				spdlog::get("global")->debug("{} set body transfer type to chunked", m_logSpec);
			}
			else
			{
				// read body and send it to client until connection is closed, then
				// close client connection as well 
				m_type = BODY_FORWARD_AND_CLOSE;
				receiver->m_type = BODY_FORWARD_AND_CLOSE;
				spdlog::get("global")->info("{} set body transfer type to forward and close",
						m_logSpec);
			}
		}
		else
		{
			size_t count = resp.headers.count("content-length");
			if (count > 1)
			{
				m_close = true;
				throw CProxyException("Multiple content-length header fields received", 
						"502");
			}
			else if (count == 1)
			{
				int size;
				try
				{
					auto it = resp.headers.find("content-length");
					size = std::stoi(it->second);
				}
				catch (std::logic_error& err) //invalid_argument or out_of_range
				{
					m_close = true;
					throw CProxyException("Non integer value passed as content-length", 
							"502");
				}
				if (size < 0)
				{
					m_close = true;
					throw CProxyException("Content-length can't be less than 0", "502");
				}
				else if (size == 0)
				{
					m_type = NORMAL;	// no body
					return;
				}

				m_bodyLength = size;
				m_sentBytes = 0;
				m_type = BODY_FORWARD;
				spdlog::get("global")->debug("{} body transfer type set to simple body forward", 
						m_logSpec);
			}
			else
			{
				// read body and send it to client until connection is closed, then
				// close client connection as well 
				m_type = BODY_FORWARD_AND_CLOSE;
				receiver->m_type = BODY_FORWARD_AND_CLOSE;
				spdlog::get("global")->info("{} set body transfer type to forward and close",
						m_logSpec);
			}
		}
	}
}

void Connection::simpleBodyForward(Connection* conn)
{
	forwardBody(conn);
	if (m_bodyLength == m_sentBytes)
	{
		resetBody();
		m_type = NORMAL; // expect new message, because body has ended
	}
	// If not all data was forwarded (body ended but there is another message in buffer)
	// handle as new message
	if (m_inBuff.getUsedSpace() > 0)
		handleMessage();
}

// read crlf
void Connection::endChunk(Connection* conn)
{
	assert(m_inBuff.getUsedSpace() > 0);
	HttpParser::Str line;
	size_t delLength;
	m_parser.setMessageBuffer(m_inBuff.getUsedStart(), m_inBuff.getUsedSpace());
	HttpParser::Status st = m_parser.getFirstLine(&line, &delLength);			
	// if chunk does not end correctly notify client and close connection
	// Not expecting trailers!
	if (st != HttpParser::EMPTY_LINE)
	{
		m_close = true;
		throw CProxyException("Chunk did not terminate correctly (not expecting trailers)",
				"400");
	}
	// server needs chunk endings too
	conn->sendAll(m_inBuff, delLength);
}
// read crlf and reset to normal transfer mode
void Connection::endChunkedTransfer(Connection* conn)
{
	endChunk(conn);
	m_type = NORMAL;
	m_newChunk = false;
}

void Connection::endCurrentChunk(Connection* conn)
{
	endChunk(conn);
	resetBody();
	m_newChunk = true;
}

void Connection::chunkedBodyForward(Connection* conn)
{
	// means we have to read new chunk
	if (m_newChunk)
	{
		size_t chunkSize, firstLineLength;
		m_parser.setMessageBuffer(m_inBuff.getUsedStart(), m_inBuff.getUsedSpace());
		HttpParser::Status st = m_parser.getChunkSize(&chunkSize, &firstLineLength);
		// If there is no full line yet return without and wait for whole line
		if (st == HttpParser::Status::NOT_ENDING)
			return;
		else if (st != HttpParser::Status::OK)
		{
			// close connection, because client is expecting us to handle body, but we don't know length
			m_close = true; 
			throw CProxyException("Failed to read chunk size", "400");
		}
		// send first line to server
		conn->sendAll(m_inBuff, firstLineLength);

		// if the last chunk, read last crlf and reset to normal transfer mode
		if (chunkSize == 0)
		{
			resetBody();
			// if there is no more data in buffer, wait for chunk termination
			if (m_inBuff.getUsedSpace() == 0)
				return;
			endChunkedTransfer(conn);
			if (m_inBuff.getUsedSpace() > 0)
				handleMessage();
			return;
		}
		m_bodyLength = chunkSize;
		m_sentBytes = 0;
		m_newChunk = false;
		// if there is no data left to read, return and wait for reamining chunk data
		if (m_inBuff.getUsedSpace() == 0)
			return;
	}
	// means we have to get ending of the final chunk in the body
	else if (m_bodyLength == 0)
	{
		endChunkedTransfer(conn);
		if (m_inBuff.getUsedSpace() > 0)
			handleMessage();
		return;
	}
	// means we have to get ending of chunk
	else if (m_bodyLength == m_sentBytes)
	{
		endCurrentChunk(conn);
		if (m_inBuff.getUsedSpace() > 0)
			handleMessage();
		return;
	}

	forwardBody(conn);
	if (m_bodyLength == m_sentBytes)
	{
		if (m_inBuff.getUsedSpace() == 0)
			return;
		endCurrentChunk(conn);		
		if (m_inBuff.getUsedSpace() > 0)
			handleMessage();
	}
	assert(m_inBuff.getUsedSpace() == 0);
}

void Connection::filterHeaders(HttpParser::Message::HeaderMap& headers)
{
	// Remove all hop by hop headers if they are present
	//https://tools.ietf.org/html/draft-ietf-httpbis-p1-messaging-14#section-7.1
	headers.erase("te");
	// Don't remove transfer encoding, because we are not changing encoding
	//headers.erase("transfer-encoding");
	headers.erase("proxy-authenticate");
	headers.erase("proxy-authorization");
	headers.erase("trailer");
	headers.erase("upgrade");
	// Remove connection headers and all headers it lists
	auto it = headers.find("connection");
	while (it != headers.end())
	{
		HttpParser::ValueList list = m_parser.getValueList(it->second);
		for (auto it = list.begin(); it != list.end(); it++)
		{
			//remove spaces and make lowercase, because that's how header names should be
			it->erase(remove_if(it->begin(), it->end(), isspace), it->end());
			std::transform(it->begin(), it->end(), it->begin(), ::tolower);
			headers.erase(*it);
		}

		headers.erase(it);
		it = headers.find("connection");
	}
	headers.erase("keep-alive");
}


void Connection::addHeaders(HttpParser::Message::HeaderMap& headers)
{
	/*
	switch (m_type)
	{
	case NORMAL:
	case BLIND_FORWARD:
	case BODY_FORWARD_AND_CLOSE:
		break;
	case CHUNKED_BODY_FORWARD:
		break;
	case BODY_FORWARD:
		break;
	}*/
}



