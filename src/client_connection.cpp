#include "client_connection.h"
#include "cproxy_exception.h"
#include "server_connection.h"
#include "cproxy.h"
#include <cassert>
#include <iostream>	//TODO: delete
#include <exception>
#include <sstream>

ClientConnection::ClientConnection(int socket, char* ip, CProxy* cproxy) :
		Connection(socket), m_ip(ip), m_proxy(cproxy)
{
	assert(cproxy != nullptr);
	m_logSpec = std::string(m_ip) + ':' + std::to_string(getSocket()) + ':';
}

#ifndef NDEBUG	
void printHeaders(HttpParser::Message* msg)
{
	for (auto it = msg->headers.begin(); it != msg->headers.end(); it++)
		std::cerr << it->first << ": " << it->second << std::endl;
}

void ClientConnection::testHandleResponse(size_t bytes)
{
	using namespace std;
	size_t parsedSize;
	HttpParser::Status st = m_parser.parseResponse(&m_response, &parsedSize);
	assert(st != HttpParser::Status::OK);
	if (st == HttpParser::Status::HEADERS_ENDED)
	{
		cerr << "version: " << m_response.version;
		cerr << endl << "status code: " << m_response.statusCode;
		cerr << endl << "reason phrase: " << m_response.reasonPhrase << endl;
		printHeaders(&m_response);
		cerr << endl << endl;
	}
	else 
	{
		cerr << "err" << endl;
		cerr.write((const char*)m_inBuff.getUsedStart(), m_inBuff.getUsedSpace());
		// only preserve message info when we haven't reached message end, and there was no
		//	formatting errors
	}

	// free parsed data
	if (parsedSize > 0)
		m_inBuff.freeFirstN(parsedSize);

	// if whole message is read or if message is malformed, reset m_response, 
	// so that next time we don't read on top of already read information
	if (st != HttpParser::Status::NOT_ENDING)
	{
		m_response = HttpParser::Response();
		// If there is still data to read, try to handle it as a new message
		if (m_inBuff.getUsedSpace() > 0)
			handleMessage();
	}
	//TODO:
	//if (st == HttpParser::Status::HEADERS_ENDED)
	//	handleBody()
	//	*/
}

void ClientConnection::testHandleRequest(size_t bytes)
{
	using namespace std;
	size_t parsedSize;
	HttpParser::Status st = m_parser.parseRequest(&m_request, &parsedSize);
	assert(st != HttpParser::Status::OK);
	if (st == HttpParser::Status::HEADERS_ENDED)
	{
		cerr << "method: " << m_request.method << endl;
		cerr << "version: " << m_request.version << endl;
		cerr << "uri: " << m_request.uri << endl;
		printHeaders(&m_request);
		cerr << endl << endl;
	}
	else
	{
		//TODO: log
		cerr << "err" << endl;
		cerr.write((const char*)m_inBuff.getUsedStart(), m_inBuff.getUsedSpace());
	}
	// free parsed data
	if (parsedSize > 0)
		m_inBuff.freeFirstN(parsedSize);

	// if whole message is read or if message is malformed, reset m_response, 
	// so that next time we don't read on top of already read information
	if (st != HttpParser::Status::NOT_ENDING)
	{
		m_request = HttpParser::Request();
		// If there is still data to read, try to handle it as a new message
		if (m_inBuff.getUsedSpace() > 0)
			handleMessage();
	}
	//TODO:
	//if (st == HttpParser::Status::HEADERS_ENDED)
	//	handleBody()
}

#endif //NDEBUG

void ClientConnection::removeServerConnection(const std::string& hostname)
{
	size_t r = m_connections.erase(hostname);
	assert(r > 0);
}

void ClientConnection::addServerConnection(ServerConnection* conn)
{
	auto r = m_connections.emplace(conn->getHostName(), conn);
	spdlog::get("global")->debug("{} {} connections", m_logSpec, m_connections.size());
	assert(r.second);
}

// Gets host name and port from authority form: 
// https://tools.ietf.org/html/draft-ietf-httpbis-p1-messaging-14#section-4.1.2
// hostValue should be value of host header
bool getHostAndPort(const std::string& hostValue, std::string& host, std::string& port)
{
	std::istringstream ss(hostValue);
	if (!std::getline(ss, host, ':'))
		return false;
	unsigned int p;
	ss >> p;
	if (ss)
		port = std::to_string(p);
	else
		port.clear();
	return true;
}


// finds connection in m_connections or creates new connection, and registers it
ServerConnection* ClientConnection::getServerConn(const std::string& hostname, const std::string& port)
{
	// first look for already available connections to this server
	auto it = m_connections.find(hostname);
	if (it != m_connections.end())
	{
		ServerConnection* serv = it->second;
		// if we already have connection to the same port, return it
		if (it->second->getPort() == port)
			return serv;
		// else remove old connection and create new, to the right port
		assert(m_proxy->removeConnection(serv));
	}
	ServerConnection* serv = ServerConnection::connectToServer(*this, hostname, port);
	spdlog::get("global")->info("{} Connected to {} on {}", m_logSpec, hostname, 
			serv->getSocket());
	m_proxy->addConnection(serv);
	return serv;
}

void ClientConnection::removeAllExcept(ServerConnection* conn)
{
	// only iterators of erased elements are invalidated for maps, so we can do this
	for (auto it = m_connections.begin(); it != m_connections.end(); it++)
	{
		if (it->second != conn)
			m_proxy->removeConnection(it->second);
	}
}


void ClientConnection::handleRequest(HttpParser::Request& req)
{
	// Get host header field value first, because it's required for all methods
	auto hostHeader = req.headers.find("host");
	if (hostHeader == req.headers.end())
	{
		throw CProxyException("Host header missing in request", "400");
	}
		
		
	std::string host, port;
	if (!getHostAndPort(hostHeader->second, host, port))
	{
		throw CProxyException(
				"Invalid host header value", "400");
	}

	if (req.method == "CONNECT")
	{
		spdlog::get("global")->info("{} connect request to {}", m_logSpec, host);
		if (port.empty())
		{
			throw CProxyException(
				"Invalid host header value",
				"Host header value in CONNECT, must have host name and port separated by colon",
				"400");
		}

		// get connection to this server
		ServerConnection* conn = getServerConn(host, port);
		// forward everything between client and server blindly
		conn->m_type = m_type = BLIND_FORWARD;
		// remove all other server connections, we won't use them as we forward anyway
		removeAllExcept(conn);
		sendResponseToClient("200", "Tunnel established");
	}
	else
	{
		spdlog::get("global")->debug("{} {} request to {}", m_logSpec, req.method, host);
		if (port.empty())
			port = "80";
		ServerConnection* conn = getServerConn(host, port);
		m_currentServ = conn;

		setBodyTransferType(req);
		filterHeaders(req.headers);
		//addHeaders(req.headers);
		conn->sendRequest(req);
	}
}

void ClientConnection::handleException(const CProxyException& exc) noexcept
{
	if (exc.getSysErrStr().empty())
		spdlog::get("global")->error("{} {}", m_logSpec, exc.what());
	else
		spdlog::get("global")->error("{} {}. {}", m_logSpec, exc.what(), 
				exc.getSysErrStr());
	const Response& resp = exc.getResponse();
	if (!resp.isEmpty())
		sendResponse(resp);
}


void ClientConnection::handleException(const std::length_error& exc) noexcept
{
	spdlog::get("global")->error("{} {}", m_logSpec, exc.what());
	//TODO: try to send out what we have already filled?
	m_outBuff.freeAll();
	sendResponseToClient("413", "Message too big to send");
}

void ClientConnection::forward()
{
	auto it = m_connections.begin();
	assert(it != m_connections.end());
	
	it->second->sendAll(m_inBuff, m_inBuff.getUsedSpace());

	spdlog::get("global")->trace("{} forwarded", m_logSpec);
}

void ClientConnection::parseAndRespond()
{
	// provide whole used buffer, not just recently read bytes
	HttpParser::MessageType type = m_parser.setMessageBuffer(m_inBuff.getUsedStart(), 
		m_inBuff.getUsedSpace());			

	HttpParser::Status st;
	// If we got wrong type of message from client
	if (m_request.isEmpty() && type == HttpParser::MessageType::RESPONSE)
	{
		sendResponseToClient("400", "Unknown method (wrong message type)");
		
		spdlog::get("global")->warn("{} {}", m_logSpec, 
				"ClientConnection::parseAndRespond: unknown method (wrong message type)");
		// free first line. Remaining data might be right.
		HttpParser::Str str;
		size_t delLength;
		st = m_parser.getFirstLine(&str, &delLength);
		if (st == HttpParser::Status::OK || st == HttpParser::Status::EMPTY_LINE)
		{
			m_inBuff.freeFirstN(str.length());
			// If there is still data to read, try to handle it as a new message
			if (m_inBuff.getUsedSpace() > 0)
				handleMessage();
		}
		else
			m_inBuff.freeAll();
	}
	else
	{
		size_t parsedSize;
		st = m_parser.parseRequest(&m_request, &parsedSize);
		// parse request should always return more information than OK
		assert(st != HttpParser::Status::OK);
		if (st == HttpParser::Status::HEADERS_ENDED)
		{
			spdlog::get("global")->debug("{} Got full message: {} {} {}", m_logSpec,
					m_request.method, m_request.uri, m_request.version);
			try
			{
				assert(!m_request.isEmpty());
				handleRequest(m_request);
			}
			catch (const CProxyException& exc)
			{
				handleException(exc);
			}
			catch (const std::length_error& exc)
			{
				handleException(exc);
			}
		}
		else
		{
			spdlog::get("global")->info("{} parseRequest returned status: {:d}",
					m_logSpec, st);
			std::string content = std::string((const char*)m_inBuff.getUsedStart(), 
					m_inBuff.getUsedSpace());
			spdlog::get("global")->debug("{} buffer: {}", m_logSpec, content);
			if (st == HttpParser::Status::BAD_FORMATING)
				sendResponseToClient("400", "Malformed message");
		}
		//// Should always execute the following block or at least free m_inBuff and reset m_request
		// free parsed data
		if (parsedSize > 0)
			m_inBuff.freeFirstN(parsedSize);

		// if whole message is read or if message is malformed, reset m_request, 
		// so that next time we don't read on top of already read information
		if (st != HttpParser::Status::NOT_ENDING)
		{
			m_request = HttpParser::Request();
			// If there is still data to read, try to handle it as a new message
			if (m_inBuff.getUsedSpace() > 0)
				handleMessage();
		}
		////////
	}
}

void ClientConnection::handleMessage()
{
	// Currently only handleRequest and sending code throw exceptions
	try
	{
		switch (m_type)
		{
		case BLIND_FORWARD:
			forward();
			break;
		case NORMAL:
			parseAndRespond();
			break;
		case BODY_FORWARD:
			simpleBodyForward(m_currentServ);
			break;
		case CHUNKED_BODY_FORWARD:
			chunkedBodyForward(m_currentServ);
			break;
		case BODY_FORWARD_AND_CLOSE:
			// not sending request because we will close client connection, 
			// when current body is transfered anyway
			spdlog::get("global")->warn("{} got message from client when in {}",
				m_logSpec, "body and forward state");
			break;
		default:
			throw CProxyException("Transfer type not implemented " + m_type);
		}
	}
	//this should thrown when message we want to send does not fit into the buffer
	catch (const std::length_error& exc) 
	{
		handleException(exc);
		spdlog::get("global")->error("{} {}", m_logSpec, 
				"exception cought on the outside, freeing reseting all data");
		m_inBuff.freeAll();
		m_request = HttpParser::Request();
	}
	catch (const CProxyException& exc)
	{
		handleException(exc);
		spdlog::get("global")->error("{} {}", m_logSpec,
				"exception cought on the outside, freeing reseting all data");
		m_inBuff.freeAll();
		m_request = HttpParser::Request();

	}
		
#ifndef NDEBUG
	//testHandleResponse(bytes);
	//testHandleRequest(bytes);
#endif
}

