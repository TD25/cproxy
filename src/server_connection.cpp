#include "server_connection.h"
#include "cproxy_exception.h"
#include "client_connection.h"
#include <iostream>

ServerConnection::ServerConnection(int socket, ClientConnection* client, 
	std::string hostname, std::string port) :
		Connection(socket), m_client(client), m_hostname(hostname), m_port(port)
{
	m_logSpec = m_hostname + ':' + std::to_string(getSocket()) + ':';
}

void ServerConnection::forward()
{
	assert(m_client);

	m_client->sendAll(m_inBuff, m_inBuff.getUsedSpace());

	spdlog::get("global")->trace("{} forwarded", m_logSpec);
}

void ServerConnection::handleResponse(Response& resp)
{
	spdlog::get("global")->debug("{} response: {} {} {}", m_logSpec, resp.version,
			resp.statusCode, resp.reasonPhrase);
	// first send headers
	// We assume we are replying to this request
	assert(m_requests.size() > 0);
	Request req = m_requests.front();
	m_requests.pop();
	setBodyTransferType(resp, req.method, m_client);
	filterHeaders(resp.headers);
	//addHeaders(resp.headers);
	m_client->sendResponse(resp);
}

void ServerConnection::parseAndSend()
{
	HttpParser::MessageType type = m_parser.setMessageBuffer(m_inBuff.getUsedStart(), 
		m_inBuff.getUsedSpace());			

	HttpParser::Status st;
	// If we got wrong type of message from client
	if (m_response.isEmpty() && type == HttpParser::MessageType::REQUEST)
	{
		m_client->sendResponseToClient("502", "Received wrong type of message from server");
		
		spdlog::get("global")->warn("{} {}. Buffer:\n{}", m_logSpec, 
				"ServerConnection::parseAndSend: wrong message type",
				std::string((char*)m_inBuff.getUsedStart(), m_inBuff.getUsedSpace()));
		// free first line. Remaining data might be right.
		HttpParser::Str str;
		size_t delLength;
		st = m_parser.getFirstLine(&str, &delLength);
		if (st == HttpParser::Status::OK || st == HttpParser::Status::EMPTY_LINE)
		{
			m_inBuff.freeFirstN(str.length());
			// If there is still data to read, try to handle it as a new message
			if (m_inBuff.getUsedSpace() > 0)
				handleMessage();
		}
		else
			m_inBuff.freeAll();
	}
	else
	{
		size_t parsedSize;
		st = m_parser.parseResponse(&m_response, &parsedSize);
		// parse request should always return more information than OK
		assert(st != HttpParser::Status::OK);
		if (st == HttpParser::Status::HEADERS_ENDED)
		{
			/*
			spdlog::get("global")->debug("{}: Got full message: {} {} {}, buffer:\n{}", 
					m_hostname, m_response.version, m_response.statusCode, m_response.reasonPhrase,
					std::string((const char*)m_inBuff.getUsedStart(), parsedSize)); */
			spdlog::get("global")->debug("{} Got full message: {} {} {}",
					m_logSpec, m_response.version, m_response.statusCode, m_response.reasonPhrase);

			try
			{
				handleResponse(m_response);
			}
			catch (const CProxyException& exc)
			{
				handleException(exc);
			}
			catch (const std::length_error& exc)
			{
				handleException(exc);
			}
		}
		else
		{
			spdlog::get("global")->info("{} parseRequest returned status: {:d}",
					m_logSpec, st);
			std::string content = std::string((const char*)m_inBuff.getUsedStart(), 
					m_inBuff.getUsedSpace());
			spdlog::get("global")->debug("{} buffer: {}", m_logSpec, content);
			if (st == HttpParser::Status::BAD_FORMATING)
				m_client->sendResponseToClient("502", "Received malformed message from server");
		}
		//// Should always execute the following block or at least free m_inBuff and reset m_response
		// free parsed data
		if (parsedSize > 0)
			m_inBuff.freeFirstN(parsedSize);

		// if whole message is read or if message is malformed, reset m_response, 
		// so that next time we don't read on top of already read information
		if (st != HttpParser::Status::NOT_ENDING)
		{
			m_response = HttpParser::Response();
			// If there is still data to read, try to handle it as a new message
			if (m_inBuff.getUsedSpace() > 0)
				handleMessage();
		}
		////
	}
}

void ServerConnection::handleMessage()
{
	try
	{
		switch (m_type)
		{
		case BLIND_FORWARD:
		case BODY_FORWARD_AND_CLOSE:
			forward();
			break;
		case NORMAL:
			parseAndSend();
			break;
		case BODY_FORWARD:
			simpleBodyForward(m_client);
			break;
		case CHUNKED_BODY_FORWARD:
			chunkedBodyForward(m_client);
			break;
		default:
			throw CProxyException("Transfer type not implemented " + m_type);
		}
	}
	catch (const std::length_error& exc)
	{
		handleException(exc);
		m_inBuff.freeAll();
		m_response = HttpParser::Response();
		spdlog::get("global")->error("{}: Freeing and reseting all data", m_logSpec);
	}
	catch (const CProxyException& exc)
	{
		handleException(exc);
		const Response& resp = exc.getResponse();
		if (!resp.isEmpty())
			m_client->sendResponse(resp);
	}
}

ServerConnection* ServerConnection::connectToServer(ClientConnection& client,
		const std::string& hostname, const std::string& port)
{
	struct addrinfo hints, *ai, *p;
    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;	//allow IPv6 as well as IPv4
    hints.ai_socktype = SOCK_STREAM;
	int rv, s_socket;
    if ((rv = getaddrinfo(hostname.c_str(), port.c_str(), &hints, &ai)) != 0)
	{
		const char* str = gai_strerror(rv);	
		throw CProxyException("ServerConnection::connectToServer: getaddrinfo failed",
				"404", "Failed to get server info: " + std::string(str), str);
    }
    
	// loop through the list of received addresses
    for(p = ai; p != NULL; p = p->ai_next) {
        s_socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (s_socket < 0) { 
            continue;
        }
        
		if (connect(s_socket, p->ai_addr, p->ai_addrlen) < 0) {
			close(s_socket);
			continue;
		}

        break;
    }

    if (p == NULL)
	{
		freeaddrinfo(ai);
		// if we got here, it means we didn't get bound
		throw CProxyException("Server::Connection::connectToServer: failed to connect",
				"404", "Failed to connect to server");
    }

    freeaddrinfo(ai); // all done with this

	return new ServerConnection(s_socket, &client, hostname, port);
}

void ServerConnection::handleException(const CProxyException& exc) noexcept
{
	if (exc.getSysErrStr().empty())
		spdlog::get("global")->error("{} {}", m_logSpec, exc.what());
	else
		spdlog::get("global")->error("{} {}. {}", m_logSpec, exc.what(), 
				exc.getSysErrStr());
	const Response& resp = exc.getResponse();
	if (!resp.isEmpty())
		sendResponse(resp);
}


void ServerConnection::handleException(const std::length_error& exc) noexcept
{
	spdlog::get("global")->error("{} {}", m_logSpec, exc.what());
	//TODO: try to send out what we have already filled?
	m_outBuff.freeAll();
	m_client->sendResponseToClient("413", "Response too big to send");
}


