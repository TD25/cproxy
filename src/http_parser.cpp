#include "http_parser.h"
#include <algorithm>
#include <stdio.h>
#include <sstream>

HttpParser::Message::~Message()
{}

HttpParser::Status HttpParser::incrementPos(size_t n)
{
	m_pos += n;
	assert(m_pos <= m_messageLen);
	//m_pos = std::min(m_pos + n, m_messageLen);
	return OK;
}

HttpParser::MessageType HttpParser::getMessageType()
{
	assert(m_message);
	// check if http version goes first
	if (strncmp((const char*)m_message, "HTTP", 4) != 0)
		m_type = REQUEST;
	else
		m_type = RESPONSE;
	return m_type;
}

HttpParser::Status HttpParser::getFirstLine(Str* pLine, size_t* delimiterLength)
{
	m_pos = 0;
	return getNextLine(pLine, delimiterLength);
}

HttpParser::Status HttpParser::getNextLine(Str* pLine, size_t* delimiterLength)
{
	char* ch = (char*)memchr(m_message + m_pos, '\n', m_messageLen - m_pos); 
	// if there is no line ending, probably we didn't get the whole message
	if (ch == NULL)
		return NOT_ENDING;
	else
	{
		// The newline character is included in newline
		*pLine = Str((char*)m_message + m_pos, ch + 1);
		if (*(--ch) == '\r')
			*delimiterLength = 2;
		else
			*delimiterLength = 1;
		// set m_pos to next byte (we expect to this line only once!)
		incrementPos(pLine->length());
		// if line is just newline character, notify caller (body might be starting)
		if (pLine->length() == *delimiterLength)
			return EMPTY_LINE;
		else
			return OK;
	}
}

HttpParser::Status HttpParser::fillMethod(Str* pLine, HttpParser::Request* request)
{
	const char* ch = (char*)memchr(pLine->getStart(), ' ', pLine->length());
	if (ch == NULL || ch == pLine->getStart())
		return BAD_FORMATING;
	else
	{
		size_t methodLen = ch - pLine->getStart();		
		request->method = std::string(pLine->getStart(), methodLen);
		return OK;
	}
}

HttpParser::Status HttpParser::fillUri(Str* pLine, HttpParser::Request* request)
{
	const char* ch = (char*)memchr(pLine->getStart(), ' ', pLine->length());
	if (ch == NULL || ch == pLine->getStart())
		return BAD_FORMATING;
	else
	{
		size_t uriLen = ch - pLine->getStart();		
		request->uri = std::string(pLine->getStart(), uriLen);
		return OK;
	}
}

HttpParser::Status HttpParser::fillVersion(Str* pLine, HttpParser::Request* request)
{
	const char* start = pLine->getStart();
	const char* ch = (char*)memchr(start, '\r', pLine->length());
	if (ch == start)
		return BAD_FORMATING;
	else if (ch == NULL && ((ch = (char*)memchr(start, '\n', pLine->length())) == NULL ||
			 ch == start))
	{
		return BAD_FORMATING;
	}
	else
	{
		size_t versionLen = ch - start;
		request->version = std::string(start, versionLen);
		return OK;
	}

}

HttpParser::Status HttpParser::fillVersion(Str* pLine, HttpParser::Response* status)
{
	const char* start = pLine->getStart();
	const char* ch = (char*)memchr(start, ' ', pLine->length());
	if (ch == NULL || ch == start)
		return BAD_FORMATING;
	else
	{
		size_t versionLen = ch - start;		
		status->version = std::string(start, versionLen);
		return OK;
	}
}

HttpParser::Status HttpParser::fillStatusCode(Str* pLine, HttpParser::Response* response)
{
	const char* start = pLine->getStart();
	const char* ch = (char*)memchr(start, ' ', pLine->length());
	if (ch == NULL || ch == start || ch - start != 3)
		return BAD_FORMATING;
	else
	{
		response->statusCode = std::string(start, 3);				
		return OK;
	}
}

HttpParser::Status HttpParser::fillReasonPhrase(Str* pLine, HttpParser::Response* status)
{
	const char* start = pLine->getStart();
	const char* ch = (char*)memchr(start, '\r', pLine->length());
	// Allowing empty phrase
	if (ch == NULL && ((ch = (char*)memchr(start, '\n', pLine->length())) == NULL))
		return BAD_FORMATING;
	else
	{
		size_t reasonPhraseLen = ch - start;
		if (reasonPhraseLen > 0)
			status->reasonPhrase = std::string(start, reasonPhraseLen);
		return OK;
	}
}

HttpParser::Status HttpParser::fillHeaders(Message* msg)
{
	// parse headers
	Status st;
	do
		st = fillNextHeader(msg);
	while (st == OK);


	if (st == EMPTY_LINE)	// means headers have ended
		return HEADERS_ENDED;
	else
		return st;
}

HttpParser::Status HttpParser::parseRequest(HttpParser::Request* request, size_t* readBytes)
{
	//TODO: refactor
	size_t delimiterLength;
	Str line;
	Status st;
	*readBytes = 0;
	// Read request line
	// Only try to read request line if this info is not already stored in *request.
	if (request->method.empty() || request->version.empty() || request->uri.empty())
	{
		Status st = getFirstLine(&line, &delimiterLength);
		if (st != OK)
		{
			if (st == EMPTY_LINE)
				*readBytes = m_messageLen;
			return st;
		}
		// find method
		st = fillMethod(&line, request);
		if (st != OK)
		{
			if (st != NOT_ENDING)
				*readBytes = m_pos;
			return st;
		}
		// don't make next fill function read method and delimiter:
		line.incrementStart(request->method.length() + 1);
		if ((st = fillUri(&line, request)) != OK)
		{
			request->method.clear();
			// Set readBytes, because we are sure that we don't want to re read it
			if (st != NOT_ENDING)
				*readBytes = m_pos;
			return st;
		}
		line.incrementStart(request->uri.length() + 1);
		if ((st = fillVersion(&line, request)) != OK)
		{
			request->method.clear();
			request->uri.clear();
			if (st != NOT_ENDING)
				*readBytes = m_pos;
			return st;
		}
	}
	
	st = fillHeaders(request);
	*readBytes = m_pos;

	return st;
}

HttpParser::Status HttpParser::parseResponse(HttpParser::Response* response, size_t* readBytes)
{
	//TODO: refactor
	size_t delimiterLength;
	Str line;
	*readBytes = 0;
	Status st;

	// Only try to read response line from buffer if it is not already stored
	if (response->version.empty() || response->statusCode.empty())
	{
		st = getFirstLine(&line, &delimiterLength);
		if (st != OK)
		{
			if (st == EMPTY_LINE)
				*readBytes = m_messageLen;
			return st;
		}
		st = fillVersion(&line, response);
		if (st != OK)
		{
			if (st != NOT_ENDING)
				*readBytes = m_pos;
			return st;
		}
		// don't make next fill function read version and delimiter:
		line.incrementStart(response->version.length() + 1);
		if ((st = fillStatusCode(&line, response)) != OK)
		{
			// remove version, because we want to read status line in one go
			response->version.clear();
			if (st != NOT_ENDING)
				*readBytes = m_pos;
			return st;
		}
		line.incrementStart(4);
		if ((st = fillReasonPhrase(&line, response)) != OK)
		{
			// clear already read info, because we want to read whole first line in one go
			response->version.clear();
			response->statusCode.clear();
			if (st != NOT_ENDING)
				*readBytes = m_pos;
			return st;
		}

	}

	st = fillHeaders(response);
	*readBytes = m_pos;

	return st;
}

HttpParser::Status HttpParser::getHeaderName(Str* pLine, Str* name)
{
	char* start = pLine->getStart();
	char* ch = (char*)memchr(start, ':', pLine->length());
	if (ch == NULL || ch == start)
		return BAD_FORMATING;
	else
	{
		size_t nameLen = ch - start;		
		// Make sure there are no spaces in header name
		void* ch1 = (char*)memchr(start, ' ', nameLen);
		if (ch1 != NULL)
			return BAD_FORMATING;
		*name = Str(start, ch);
		return OK;
	}
}

HttpParser::Status HttpParser::getHeaderValue(Str* pLine, Str* value)
{
	char* start = pLine->getStart();
	char* ch = (char*)memchr(start, '\r', pLine->length());
	if (ch == start)
		return BAD_FORMATING;
	else if (ch == NULL && ((ch = (char*)memchr(start, '\n', pLine->length())) == NULL ||
				ch == start))
	{
		return BAD_FORMATING;
	}
	else
	{
		// Allow space after colon
		if (*start == ' ')
			start++;
		*value = Str(start, ch);
		return OK;
	}
}

HttpParser::Status HttpParser::fillNextHeader(Message* message)
{
	// if this function got called when we are at the end of the buffer,
	// we assume that proper ending hasn't been reached.
	if (m_pos >= m_messageLen)
		return NOT_ENDING;
	// get whole line first
	Str line;
	size_t delimiterLength;
	// assuming m_pos points to next header
	Status st = getNextLine(&line, &delimiterLength);
	if (st != OK)
		return st;
	else
	{
		Str name, value;
		// get header name first
		if ((st = getHeaderName(&line, &name)) != OK)
			return st;
		// Make header name lowercase (http headers are case insensitive)
		name.toLower();

		// look for value after colon
		line.incrementStart(name.length() + 1);
		if ((st = getHeaderValue(&line, &value)) != OK)
			return st;
		
		message->headers.emplace(std::string(name.getStart(), name.length()), 
				std::string(value.getStart(), value.length()));
		//message->headers.emplace(name, value);
		return OK;
	}
}

HttpParser::Status HttpParser::packRequest(const Request* request, uint8_t* buffer, 
		size_t bufferSize, size_t* usedBytes)
{
	assert(!request->isEmpty());
	// Store status line first
	*usedBytes = 0;
	int r = snprintf((char*)buffer, bufferSize, "%s %s %s\r\n", request->method.c_str(),
			request->uri.c_str(), request->version.c_str());
	if (r >= bufferSize)
	{
		throw std::length_error(
				"HttpParser::packResponse: buffer too small to create status line");
	}

	// r does not include \0
	*usedBytes += r;
	size_t sizeLeft = bufferSize - *usedBytes;
	size_t used;
	packHeaders(request, buffer+*usedBytes, sizeLeft, &used);
	*usedBytes += used;
	return OK;
}

HttpParser::Status HttpParser::packResponse(const Response* response, uint8_t* buffer, 
		size_t bufferSize, size_t* usedBytes)
{
	assert(!response->isEmpty());
	// Store status line first
	*usedBytes = 0;
	int r = snprintf((char*)buffer, bufferSize, "%s %s %s\r\n", response->version.c_str(),
			response->statusCode.c_str(), response->reasonPhrase.c_str());
	if (r >= bufferSize)
	{
		throw std::length_error(
				"HttpParser::packResponse: buffer too small to create status line");
	}

	// r does not include \0
	*usedBytes += r;
	size_t sizeLeft = bufferSize - *usedBytes;
	size_t used;
	packHeaders(response, buffer+*usedBytes, sizeLeft, &used);
	*usedBytes += used;
	return OK;
}

HttpParser::Status HttpParser::packHeaders(const Message* msg, uint8_t* buffer, size_t bufferSize,
			size_t* usedBytes)
{
	*usedBytes = 0;
	for (auto it = msg->headers.begin(); it != msg->headers.end(); it++)
	{
		assert(!it->first.empty());
		int r = snprintf((char*)(buffer+*usedBytes), bufferSize - *usedBytes, "%s: %s\r\n", 
				it->first.c_str(), it->second.c_str());
		if (r >= bufferSize)
			throw std::length_error("HttpParser::packHeaders: buffer too small to add all headers");
		*usedBytes += r;
	}

	size_t sizeLeft = bufferSize - *usedBytes;
	if (sizeLeft < 2)
		throw std::length_error("HttpParser::packHeaders: buffer too small to end headers");
	strncpy((char*)(buffer+*usedBytes), "\r\n", 2);
	*usedBytes += 2;
	return OK;
}

HttpParser::ValueList HttpParser::getValueList(const std::string& headerValue)
{
	ValueList list;
	std::istringstream stream(headerValue);
	std::string value;
	// works
	while (std::getline(stream, value, ','))
		list.push_back(value);
	return list;
}

HttpParser::ValueList HttpParser::findValueList(
		const Message::HeaderMap& headers, const std::string& headerName)
{
	ValueList list;
	// merge value lists in all header fields with this name
	auto its = headers.equal_range(headerName);	
	for (auto it = its.first; it != its.second; it++)
		list.splice(list.begin(), getValueList(it->second));
	return list;
}

HttpParser::Status HttpParser::getChunkSize(size_t* size, size_t* firstLineLength)
{
	Str line;
	size_t delLength;
	Status st = getFirstLine(&line, &delLength);
	if (st == EMPTY_LINE)
		return BAD_FORMATING;
	else if (st != OK)
		return st;
	char* end;
	long int i = strtol(line.getStart(), &end, 16);
	// means no digits were found
	if (line.getStart() == end) 
		return BAD_FORMATING;
	//FIXME: check if it didn't overflow, underflow and fits into size_t
	*size = i;
	*firstLineLength = line.length();
	return OK;
}


