CC=g++
INDIR=include
OBJDIR=obj
BINDIR=bin
SRCDIR=src
DEPS=$(INDIR)/common.h $(INDIR)/cproxy_exception.h
LOGDIR=$(BINDIR)/logs
CFLAGS=-I$(INDIR) -Wall -g -std=c++11 -pthread

_OBJ = main.o cproxy.o connection.o client_connection.o server_connection.o sbuffer.o http_parser.o
OBJ = $(patsubst %,$(OBJDIR)/%,$(_OBJ))

all: dirs $(BINDIR)/cproxy

dirs: $(OBJDIR) $(BINDIR) $(LOGDIR)

$(OBJDIR):
	mkdir $(OBJDIR)
$(BINDIR):
	mkdir $(BINDIR)
$(LOGDIR):
	mkdir $(LOGDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp  $(INDIR)/%.h $(DEPS)
	$(CC) -c -o $@ $<  $(CFLAGS)

$(OBJDIR)/main.o: $(SRCDIR)/main.cpp $(DEPS)
	$(CC) -c -o $(OBJDIR)/main.o $< $(CFLAGS)

$(BINDIR)/cproxy: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm $(BINDIR)/cproxy $(OBJDIR)/*.o

run: dirs $(BINDIR)/cproxy
	$(BINDIR)/cproxy


