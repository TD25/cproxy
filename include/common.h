#ifndef COMMON_H_
#define COMMON_H_

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <exception>

#define HOSTNAME_LENGTH 256
#define BUFFER_LENGTH 64000
#define PORT "8080"      // the port users will be connecting to
#define BACKLOG 20     // how many pending connections queue will hold
#define TIMEOUT 300

// get sockaddr, IPv4 or IPv6:
inline void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

inline void logException(const std::exception& exc)
{
}


#endif
