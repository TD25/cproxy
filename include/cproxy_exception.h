#ifndef CPROXY_EXCEPTION_H_
#define CPROXY_EXCEPTION_H_

#include "http_parser.h"
#include <stdexcept>

class CProxyException : public std::runtime_error
{
private:
	std::string m_systemErr;	// string from system (strerror or gai_strerror)
	HttpParser::Response m_resp;	// response to client to send
public:
	CProxyException(std::string whatArg, std::string systemErrStr, HttpParser::Response resp) :
		std::runtime_error(whatArg), m_systemErr(std::move(systemErrStr)),
		m_resp(std::move(resp))
	{}
	CProxyException(std::string whatArg) : 
		CProxyException(whatArg, std::string(), HttpParser::Response())
	{}
	// creates response for user to send with specified status code and whatArg as reason phrase
	// systemErrStr - should be string returned by strerror, or gai_strerror...
	CProxyException(std::string whatArg, std::string statusCode, 
		std::string systemErrStr = std::string()) : 
			CProxyException(std::string(whatArg), std::move(systemErrStr), 
				HttpParser::Response(std::move(statusCode), "HTTP/1.1", std::string(whatArg)))
	{}
	CProxyException(std::string whatArg, std::string statusCode, std::string reasonPhrase,
		std::string systemErrStr) : 
			CProxyException(std::move(whatArg), std::move(systemErrStr), 
				HttpParser::Response(std::move(statusCode), "HTTP/1.1",
				std::move(reasonPhrase)))
	{}

	const HttpParser::Response& getResponse() const noexcept
	{
		return m_resp;
	}

	const std::string& getSysErrStr() const noexcept
	{
		return m_systemErr;
	}

	void setResponse(HttpParser::Response resp)
	{
		m_resp = std::move(resp);
	}

	void setSystemErrStr(std::string systemErrStr)
	{
		m_systemErr = std::move(systemErrStr);
	}
};

#endif
