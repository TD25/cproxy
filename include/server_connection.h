#ifndef SERVER_CONNECTION_H_
#define SERVER_CONNECTION_H_

#include "cproxy_exception.h"
#include <connection.h>
#include <string>
#include <stdexcept>
#include <queue>

class ClientConnection;

class ServerConnection : public Connection
{
	friend class ClientConnection;
private:
	ClientConnection* m_client;
	std::string m_hostname;
	std::string m_port;
	std::queue<Request> m_requests;

	void forward();
	void parseAndSend();
	void handleResponse(Response& response);
	void handleException(const CProxyException& exc) noexcept;
	void handleException(const std::length_error& exc) noexcept;
public:
	ServerConnection(int socket, ClientConnection* client, std::string hostname, 
			std::string port);
	// Tries to connect to specified server and create ServerConnection object (with new)
	// Throws CProxyException on failure to do so
	static ServerConnection* connectToServer(ClientConnection& client, 
			const std::string& hostname, const std::string& port);
	virtual ~ServerConnection() {}
	void handleMessage();
	std::string getHostName()
	{
		return m_hostname;
	}
	std::string getPort()
	{
		return m_port;
	}
	virtual void sendRequest(Request request)
	{
		Connection::sendRequest(request);
		m_requests.push(std::move(request));
	}
};


#endif //SERVER_CONNECTION_H_

