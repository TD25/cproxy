#ifndef SBUFF_H_
#define SBUFF_H_

#include <cstdint>
#include <cassert>
#include "common.h"

 // Smart socket buffer. Keeps track of currently used space.
class SBuffer
{
private:
	uint8_t  m_buff[BUFFER_LENGTH];
	uint8_t* m_start;
	uint8_t* m_end;
public:
	SBuffer() 
	{
		m_start = m_buff;
		m_end = m_buff + BUFFER_LENGTH - 1;
	}
	~SBuffer() {}

	// Write to file. Returns value returned by c write function
	// Registers writen space as unused
	ssize_t writeTo(int fildes, size_t count);
	// Read from file into the buffer. Returns value returned by c read function
	// Registers used space in reading as used
	ssize_t readFrom(int fildes, size_t count);
	// Makes sure to write all count bytes
	// Returns true on success. If false, errno should be set by write.
	bool writeAllTo(int fildes, size_t count, size_t* writtenBytes);
	// Like writeAllTo, except internally uses send instead of write function and
	// you can specify flags to pass to send
	bool sendAllTo(int fildes, size_t count, size_t* writtenBytes, int flags);
	// Returns pointer to the begining of free data
	uint8_t* getFreeStart() { return m_start; }
	// Returns pointer to the end of free data
	uint8_t* getFreeEnd() { return m_end; }
	// Returns pointer to the start of used data
	uint8_t* getUsedStart() { return m_buff; }
	uint8_t* getUsedEnd() 
	{ 
		assert(m_start < m_buff);
		return m_start > m_buff ? m_start - 1 : m_start;
	}
	// Returns number of unused bytes in buffer
	size_t getFreeSpace()
	{
		return m_end - m_start;
	}
	size_t getUsedSpace()
	{
		return m_start - m_buff;
	}
	// Frees first n used bytes in buffer and puts the rest in the begining
	void freeFirstN(size_t n);
	void freeAll()
	{
		if (getUsedSpace() > 0)
			freeFirstN(getUsedSpace());
	}
	// Registers first free n bytes as used
	void useFirstN(size_t n)
	{
		assert(getFreeSpace() >= n);
		m_start += n;
	}
};
#endif	//SBUFF_H_
