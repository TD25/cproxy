#ifndef CLIENT_CONNECTION_H_
#define CLIENT_CONNECTION_H_

#include "connection.h"
#include "cproxy_exception.h"
#include <stdexcept>
#include <sys/socket.h>
#include <unordered_map>
#include <string>

class ServerConnection;

class ClientConnection : public Connection
{
private:
	//TODO: list of open requests
	std::unordered_map<std::string, ServerConnection*> m_connections; // open connections to servers
	char* m_ip;
	CProxy* m_proxy;
	ServerConnection* m_currentServ = nullptr;	// needed when transfering body
#ifndef NDEBUG
	void testHandleRequest(size_t bytes);
	void testHandleResponse(size_t bytes);
#endif //NDEBUG
	void handleRequest(HttpParser::Request& req);

	void handleException(const CProxyException& exc) noexcept;
	void handleException(const std::length_error& exc) noexcept;
	void removeAllExcept(ServerConnection* conn);
	// finds connection in m_connections or creates new connection, and registers it
	ServerConnection* getServerConn(const std::string& hostname, const std::string& port);
	void forward();	//should be called when handling messages by blind forwarding
	void parseAndRespond();	//should be called when handling messages normaly
public:
	ClientConnection(int socket, char* ip, CProxy* m_cproxy);
	virtual ~ClientConnection() {}
	void handleMessage();
	void removeServerConnection(const std::string& hostname);
	void addServerConnection(ServerConnection* conn);
	void sendResponseToClient(const std::string& statusCode, const std::string& reasonPhrase)
	{
		return sendResponse(HttpParser::Response(statusCode, "HTTP/1.1", reasonPhrase));
	}
};


#endif

