#ifndef HTTP_PARSER_H_
#define HTTP_PARSER_H_

#include <cstdint>
#include <cstring>
#include <string>
#include <unordered_map>
#include <cassert>
#include <stdexcept>
#include <cctype>
#include <list>

class HttpParser
{
public:
	// NOT_ENDING - should be set if message does not end in current buffer
	// EMPTY_LINE - should be set if parser found empty line (used internally)
	enum Status { OK, EMPTY_LINE, HEADERS_ENDED, BAD_FORMATING, NOT_ENDING, 
		WRONG_TYPE, BODY_ENDED };
	enum MessageType { REQUEST, RESPONSE };
	
	typedef std::list<std::string> ValueList; 

	class Message
	{
	public:
		typedef std::unordered_multimap<std::string, std::string> HeaderMap;
		std::string version;
		// keys - header names, values - header values
		HeaderMap headers;

		Message(std::string version) : version(std::move(version))
		{}
		Message() : Message(std::string())
		{}
		// Make into abstract class
		virtual ~Message() = 0;

		// Returns true if doesn't have required information stored
		virtual bool isEmpty() const
		{
			return version.empty();
		}
	};

	class Request : public Message
	{
	public:
		std::string method;
		std::string uri;

		Request(std::string method, std::string uri, std::string version) :
			Message(std::move(version)), method(std::move(method)), uri(std::move(uri))
		{}
		Request() : Request(std::string(), std::string(), std::string())
		{}

		virtual bool isEmpty() const
		{
			return method.empty() || uri.empty() || Message::isEmpty();
		}
	};

	class Response : public Message
	{
	public:
		std::string statusCode;		//FIXME: make into int
		std::string reasonPhrase;

		Response(std::string statusCode, std::string version, 
				std::string reasonPhrase = std::string()) : Message(std::move(version)),
				statusCode(std::move(statusCode)), reasonPhrase(std::move(reasonPhrase))
		{}	
		Response() : Response(std::string(), std::string(), std::string())
		{}

		virtual bool isEmpty() const
		{
			return statusCode.empty() || Message::isEmpty();
		}
	};
	
	class Str
	{
	private:
		char* m_start;
		char* m_end; // one past the end
	public:
		Str() : m_start(nullptr), m_end(nullptr) 
		{}
		// end - should be one past the last character of string
		Str(char* start, char* end)
		{
			assert(start <= end);
			m_start = start;
			m_end = end;
		}
		Str(char* start, size_t length)
		{
			assert(length >= 0);
			m_start = start;
			m_end = m_start + length;
		}

		size_t length()
		{
			return m_end - m_start;
		}
		char* getLast()
		{
			return m_end - 1;
		}
		char* getStart()
		{
			return m_start;
		}
		char* getEnd()
		{
			return m_end;
		}
		// The following functions fail if trying to set invalid values!
		void setStart(char* start)
		{
			assert(start <= m_end);
			m_start = start;
		}
		void setEnd(char* end)
		{
			assert(end >= m_start);
			m_end = end;
		}
		void incrementStart(size_t n)
		{
			assert(m_start + n <= m_end);
			m_start += n;
		}
		void toLower()
		{
			for (char* p = m_start; p < m_end; p++)
				*p = tolower(*p);
		}
	};

private:

	// buffer where the message should be
	const uint8_t* m_message;
	size_t m_messageLen;
	size_t m_nextLineNo;
	MessageType m_type;
	size_t m_pos;		// position in m_message buffer

	Status fillMethod(Str* pLine, Request* request);
	Status fillUri(Str* pLine, Request* request);
	Status fillVersion(Str* pLine, Request* request);
	Status fillVersion(Str* pLine, Response* status);
	Status fillStatusCode(Str* pLine, Response* status);
	Status fillReasonPhrase(Str* p, Response* status);
	Status getHeaderName(Str* l, Str* name);
	Status getHeaderValue(Str* l, Str* value);

	// Header names filled are lowercase (http header name are case insensitive)
	Status fillNextHeader(Message* message);
	Status fillHeaders(Message* msg);

	Status incrementPos(size_t n);

	size_t getNextLineNo()
	{
		return m_nextLineNo;
	}

	Status packHeaders(const Message* msg, uint8_t* buffer, size_t bufferSize,
			size_t* usedBytes);
public:
	MessageType getMessageType();
	MessageType setMessageBuffer(const uint8_t* message, size_t messageLen)
	{
		m_message = message;
		m_messageLen = messageLen;
		m_nextLineNo = 0;
		m_pos = 0;
		return getMessageType();
	}

	// set pointer to point to line in buffer and length including delimiter
	// Updates m_pos. If successful returns OK or EMPTY_LINE, otherwise NOT_ENDING most likely.
	Status getFirstLine(Str* pLine, size_t* delimiterLength);
	Status getNextLine(Str* pLine, size_t* delimiterLength);

	// readBytes is filled with number of bytes read from buffer and put into 
	//  request. It specifies how much of a buffer can be freed, and won't
	//  be needed to parse when full message arrives. You can ignore it when Status
	//  is OK, that is when full message was read, you can free whole buffer.
	//
	// These 2 functions only fill first line info and headers. Call getBody to get body.
	//
	// They should never return OK. Either NOT_ENDING or HEADERS_ENDED if everything is ok
	//
	// It's responsibilit of the caller to call the right one of these two. 
	// Depending of what type of massage is in the message buffer.
	Status parseRequest(Request* request, size_t* readBytes);
	Status parseResponse(Response* response, size_t* readBytes);

	// Formats message and stores it in buffer. 
	// usedBytes - set to how many bytes of buffer is used to store message
	// can throw length_error if buffer is not big enough
	Status packRequest(const Request* request, uint8_t* buffer, size_t bufferSize,
			size_t* usedBytes);
	Status packResponse(const Response* request, uint8_t* buffer, size_t bufferSize,
			size_t* usedBytes);

	// extracts values separated by a comma
	ValueList getValueList(const std::string& headerValue);
	// finds all fields with specified header name and puts all of their values in the single list
	ValueList findValueList(const Message::HeaderMap& headers, const std::string& headerName);

	// reads first line of chunk to get chunk size
	// be sure to call setMessageBuffer first
	// buffer start + firstLineLength should be the start of the chunk-data
	Status getChunkSize(size_t* size, size_t* firstLineLength);
};

#endif
