#ifndef CPROXY_H_
#define CPROXY_H_
#include <sys/socket.h>
#include <memory>
#include "connection.h"
#include "spdlog/spdlog.h"
#include "common.h"

// proxy for single client
class CProxy
{
private:
	std::shared_ptr<spdlog::logger> m_logger;
	std::unordered_map<int, Connection*> m_connections;
	int m_clientSocket;
	sockaddr_storage m_clientAddr;
	char m_remoteIP[INET6_ADDRSTRLEN];
	fd_set m_socketSet;
	int m_fdmax = 0;

	int findBiggestFileDescriptor() const;
public:
	CProxy(int socket, sockaddr_storage clientAddr) : m_clientSocket(socket),
		m_clientAddr(clientAddr)
	{
		m_logger = spdlog::get("global");
		inet_ntop(clientAddr.ss_family, get_in_addr((struct sockaddr*)&clientAddr),
             m_remoteIP, INET6_ADDRSTRLEN);
	}
	~CProxy();
	// Pass pointer to connection created with new
	void addConnection(Connection* connection);
	// returns false if thread should terminate
	bool removeConnection(int socket);
	bool removeConnection(Connection* connection);
	void removeAllConnections();
	void serviceClient();
};

#endif //CPROXY_H_
