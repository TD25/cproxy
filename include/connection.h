#ifndef CONNECTION_H_
#define CONNECTION_H_

#include <unordered_map>
#include "common.h"
#include "http_parser.h"
#include "sbuffer.h"
#include "spdlog/spdlog.h"

class CProxy;

class Connection
{
public:
	typedef HttpParser::Request Request;
	typedef HttpParser::Response Response;
	enum TransmissionType { NORMAL, BLIND_FORWARD, BODY_FORWARD, CHUNKED_BODY_FORWARD, 
		BODY_FORWARD_AND_CLOSE };	//forwards body of response from server and closes connection
private:
	int m_socket;
protected:
	std::string m_logSpec;	// should be filled by derived classes
	SBuffer m_inBuff;
	SBuffer m_outBuff;
	HttpParser m_parser;
	HttpParser::Request m_request;
	HttpParser::Response m_response;
	TransmissionType m_type = NORMAL;
	// for receiving bodies
	// In case of chunked transfer this represents chunk length
	size_t m_bodyLength = 0;
	size_t m_sentBytes = 0;
	bool m_newChunk = false;
	bool m_close = false;

	// sets m_type to one of body transmission types and sets bodyLength and sentBytes if needed.
	// Also might modify message if needed and might set m_close
	void setBodyTransferType(Request& request);
	void setBodyTransferType(Response& response, const std::string& reqMethod, 
			Connection* receiver);
	void forwardBody(Connection* connection);
	void simpleBodyForward(Connection* connection);
	void chunkedBodyForward(Connection* connection);
	void endChunkedTransfer(Connection* connection);
	void endChunk(Connection* conn);
	void endCurrentChunk(Connection* conn);
	void filterHeaders(HttpParser::Message::HeaderMap& headers);
	void resetBody()
	{
		m_bodyLength = m_sentBytes = 0;
	}
public:
	Connection(int socket);
	virtual ~Connection();
	int getSocket() const
	{
		return m_socket;
	}
	TransmissionType getTransmissionType()
	{
		return m_type;
	}
	ssize_t receiveMessage();
	virtual void sendRequest(const Request& request);
	void sendResponse(const Response& response);
	// Send bytes amount of used buffer.
	// Sends all bytes or throws CProxyException with system error string set.
	void sendAll(SBuffer& buffer, size_t bytes);
	// adds headers to specify body transfer type. Uses m_type to determine that.
	void addHeaders(HttpParser::Message::HeaderMap& headers);
	virtual void handleMessage() = 0;
	// set if it was determined that connection should closed while handling message
	bool toClose() 
	{
		return m_close;
	}
};

#endif
